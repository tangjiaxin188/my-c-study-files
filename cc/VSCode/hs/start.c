#include "raylib.h"
#include "stdio.h"
#include "stdlib.h"
#include "ui.c"
#define WIN_WIDE 800
#define WIN_HIGHT 600

int startgame_WINDOWS();

int startgame_WINDOWS()
{
    char mousex[4] = "9999";
    char mousey[4] = "9999";

    InitWindow(WIN_WIDE,WIN_HIGHT,"test");
    

    while (!WindowShouldClose())
    {
       BeginDrawing();
       
       ClearBackground(WHITE);

       int mox = GetMouseX();
       int moy = GetMouseY();

       itoa(mox,mousex,10);
       itoa(moy,mousey,10);

       DrawText(mousex,10,10,20,RED);
       DrawText(mousey,10,30,20,RED);

       int startyn;
       Button startbutton;//621,2019
       startbutton.Hight = (621/6000)*WIN_HIGHT;
       startbutton.Wide = (2019/8000)*WIN_WIDE;
       startbutton.buttonimage = LoadImage("../res/start.png");

       int exityn;
       Button exitbutton;//621,2019
       exitbutton.Hight = (621/6000)*WIN_HIGHT;
       exitbutton.Wide = (2019/8000)*WIN_WIDE;
       exitbutton.buttonimage = LoadImage("../res/exit.png");

       startyn = ButtonPut(startbutton,(2990/8000)*WIN_WIDE,(2117/6000)*WIN_HIGHT);//405,2990,2177
       exityn = ButtonPut(exitbutton,(2990/8000)*WIN_WIDE,((2117+405)/6000)*WIN_HIGHT);

       if ( exityn == 0){
            WindowShouldClose();
            return 1;
       }

       if ( startyn == 0){
            WindowShouldClose();
            return 2;
       }

       UnloadImage(startbutton.buttonimage);
       UnloadImage(exitbutton.buttonimage);
       
       EndDrawing();

    }

}