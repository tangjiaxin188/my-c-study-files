#include "raylib.h"

typedef struct Button_1
{
    int Wide;
    int Hight;
    Image buttonimage;
} Button;

int ButtonPut(Button button,int x,int y){

    BeginDrawing();
    Texture bi = LoadTextureFromImage(button.buttonimage);
    DrawTexture(bi,x,y,WHITE);
    if ( IsMouseButtonPressed(MOUSE_BUTTON_LEFT) == true && GetMouseX() >= x && GetMouseX() <= x + button.Wide && GetMouseY() >= y && GetMouseY() <= y + button.Hight){
        EndDrawing();
        return 0;
    }
    EndDrawing();
    return 1;
}